/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.restoremetadata.model;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import chabernac.restoremetadata.utils.FileUtil;

public class RestoreMetaData implements Runnable {
    private final File     myInfoFile;
    private final File     myDirectory;
    private final String[] myExtensions;
    private final String[] myExcludedDirs;

    public RestoreMetaData( File aDirectory, String[] aExtensions, String[] aExcludedDirs ) {
        super();
        myDirectory = aDirectory;
        myExtensions = aExtensions;
        myInfoFile = new File( aDirectory, "fileinfo.bin" );
        // System.out.println( "Will write/read meta data to '" + myInfoFile.getAbsolutePath() + "'" );
        myExcludedDirs = aExcludedDirs;
    }

    @Override
    public void run() {
        try {
            Map<String, FileInfo> theStoredFileInfo = FileUtil.loadFileInfo( myInfoFile );
            Map<String, FileInfo> theFileInfo = FileUtil.getFilesForDirectory(
                    myDirectory.getAbsolutePath(),
                    myExtensions,
                    myExcludedDirs );

            for ( FileInfo theFInfo : theFileInfo.values() ) {
                if ( theStoredFileInfo.containsKey( theFInfo.getKey() ) ) {
                    // this file with the same signature already existed, restore the file info
                    // the loation of the file might have changes update the location in the store
                    FileInfo theInfo = theStoredFileInfo.get( theFInfo.getKey() );
                    theInfo.setFile( theFInfo.getFile() );
                    theInfo.restore();
                } else {
                    theStoredFileInfo.put( theFInfo.getKey(), theFInfo );
                }
            }
            FileUtil.saveFileInfo( myInfoFile, theStoredFileInfo );
        } catch ( IOException e ) {
            throw new RuntimeException( "Error occured while running restore metadata", e );
        }

    }

}
