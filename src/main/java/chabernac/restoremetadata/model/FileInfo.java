/**
 * Copyright (c) 2013 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.restoremetadata.model;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileInfo implements Serializable {
    private static final SimpleDateFormat DATE_FORMAT      = new SimpleDateFormat( "dd/MM/yyyy HH:mm:ss" );
    private static final long             serialVersionUID = 6338053959082905754L;
    private File                          file;
    private final String                  hash;
    private final long                    timestamp;

    public FileInfo( File aFile, String aHash ) {
        super();
        file = aFile;
        hash = aHash;
        timestamp = aFile.lastModified();
    }

    public File getFile() {
        return file;
    }

    public String getHash() {
        return hash;
    }

    public String getKey() {
        return file.getName() + "_" + hash;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setFile( File aFile ) {
        file = aFile;
    }

    public void restore() {
        file.setLastModified( timestamp );
    }

    @Override
    public String toString() {
        return file.getName() + " " + hash + " " + DATE_FORMAT.format( new Date( timestamp ) );
    }

}
