/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.restoremetadata.model;

import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.AndFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.NameFileFilter;
import org.apache.commons.io.filefilter.NotFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;

public class FileFilterFactory {
    public static IOFileFilter createFileFilter( String[] aExtensions ) {
        return new SuffixFileFilter( toSuffixes( aExtensions ) );
    }

    public static IOFileFilter createDirectoryFilter( String[] aExcludedDirs ) {
        AndFileFilter theExcludedDirs = new AndFileFilter();
        if ( aExcludedDirs != null ) {
            for ( String theExclusion : aExcludedDirs ) {
                theExcludedDirs.addFileFilter( new NotFileFilter( new NameFileFilter(
                        theExclusion,
                        IOCase.INSENSITIVE ) ) );
            }
        }
        return theExcludedDirs;

    }

    private static String[] toSuffixes( String[] extensions ) {
        String[] suffixes = new String[ extensions.length ];
        for ( int i = 0; i < extensions.length; i++ ) {
            suffixes[ i ] = "." + extensions[ i ];
        }
        return suffixes;
    }
}
