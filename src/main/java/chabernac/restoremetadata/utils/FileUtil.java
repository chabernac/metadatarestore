/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.restoremetadata.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import chabernac.restoremetadata.model.FileFilterFactory;
import chabernac.restoremetadata.model.FileInfo;

public class FileUtil {
    public static Map<String, FileInfo> getFilesForDirectory( String aDirectory, String[] aExtentions, String[] anExcludedDirectories ) {
        Map<String, FileInfo> theHashes = new HashMap<String, FileInfo>();

        Collection<File> theFiles = FileUtils.listFiles(
                new File( aDirectory ),
                FileFilterFactory.createFileFilter( aExtentions ),
                FileFilterFactory.createDirectoryFilter( anExcludedDirectories ) );

        for ( File theFile : theFiles ) {
            FileInputStream theFileInpustream = null;
            try {
                theFileInpustream = new FileInputStream( theFile );
                String theHash = DigestUtils.md5Hex( IOUtils.toByteArray( theFileInpustream ) );
                // System.out.println( theFile + "[" + theHash + "]" );
                FileInfo theFileInfo = new FileInfo( theFile, theHash );
                theHashes.put( theFileInfo.getKey(), new FileInfo( theFile, theHash ) );
            } catch ( Exception e ) {
                e.printStackTrace();
            } finally {
                if ( theFileInpustream != null ) {
                    try {
                        theFileInpustream.close();
                    } catch ( IOException e ) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return theHashes;
    }

    public static void saveFileInfo( File aFile, Map<String, FileInfo> aFileInfo ) throws IOException {
        try (ObjectOutputStream theOut = new ObjectOutputStream( new GZIPOutputStream( new FileOutputStream( aFile ) ) )) {
            theOut.writeObject( aFileInfo );
        }
    }

    public static Map<String, FileInfo> loadFileInfo( File aFile ) throws IOException {
        if ( !aFile.exists() ) return new HashMap<>();
        try (ObjectInputStream theInput = new ObjectInputStream( new GZIPInputStream( new FileInputStream( aFile ) ) )) {
            return (Map<String, FileInfo>) theInput.readObject();
        } catch ( ClassNotFoundException e ) {
            throw new IOException( "Unable to load file info", e );
        }
    }

}
