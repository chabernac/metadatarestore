/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.restoremetadata.main;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;

import chabernac.restoremetadata.model.FileInfo;
import chabernac.restoremetadata.utils.FileUtil;

public class Print {
    public static void main( String[] args ) throws IOException {
        IOFileFilter theFilter = null;
        if ( args.length >= 2 ) {
            theFilter = new RegexFileFilter( Pattern.compile( args[ 1 ] ) );
        }
        for ( FileInfo theInfo : FileUtil.loadFileInfo( new File( args[ 0 ], "fileinfo.bin" ) ).values() ) {
            if ( theFilter == null || theFilter.accept( theInfo.getFile() ) ) {
                System.out.println( theInfo );
            }
        }
    }
}
