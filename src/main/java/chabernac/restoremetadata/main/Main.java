/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.restoremetadata.main;

import java.io.File;

import chabernac.restoremetadata.model.RestoreMetaData;

public class Main {
    public static void main( String[] args ) {
        // for ( String arg : args ) {
        // System.out.println( "Arg: '" + arg + "'" );
        // }
        if ( args.length < 2 ) {
            System.out.println( "Usage: [directory] [file extentions ,-seperated]" );
            System.exit( -1 );
        }

        String theDirectory = args[ 0 ];

        if ( theDirectory == null || theDirectory.length() == 0 ) {
            exit( "Not a valid directory: '" + theDirectory + "'" );
        }

        String[] theExtentions = null;
        if ( args.length >= 2 ) {
            theExtentions = args[ 1 ].split( ":" );
        }

        String[] theExcludedDirs = null;
        if ( args.length >= 3 ) {
            theExcludedDirs = args[ 2 ].split( ":" );
        }

        if ( theExtentions.length == 0 ) {
            exit( "At least 1 file extension must be provided" );
        }

        new RestoreMetaData( new File( args[ 0 ] ), theExtentions, theExcludedDirs ).run();
    }

    private static void exit( String aMessage ) {
        System.out.println( aMessage );
        System.exit( -1 );
    }
}
