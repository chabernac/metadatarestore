/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.restoremetadata.model;

import java.io.File;
import java.io.FileFilter;

import org.junit.Assert;
import org.junit.Test;

public class FileFilterFactoryTest {
    @Test
    public void testDirectoryFilter() {
        File theTarget = new File( "target" );
        Assert.assertTrue( theTarget.exists() );
        File theSrc = new File( "src" );
        Assert.assertTrue( theSrc.exists() );

        FileFilter theFilter = FileFilterFactory.createDirectoryFilter( new String[] { "target", "bin" } );
        Assert.assertFalse( theFilter.accept( theTarget ) );
        Assert.assertTrue( theFilter.accept( theSrc ) );

    }

    @Test
    public void testFileFilter() {
        File thePom = new File( "pom.xml" );
        Assert.assertTrue( thePom.exists() );
        File theProject = new File( ".project" );
        Assert.assertTrue( theProject.exists() );

        FileFilter theFilter = FileFilterFactory.createFileFilter( new String[] { "xml" } );
        Assert.assertTrue( theFilter.accept( thePom ) );
        Assert.assertFalse( theFilter.accept( theProject ) );
    }

}
